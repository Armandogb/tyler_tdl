<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->insert([
            'name' => "Jim",
            'email' => 'something@gmail.com',
            'password' => bcrypt('secret'),
        ]);

        DB::table('ulists')->insert([
            'name' => "todo",
            'user_id' => 1     
        ]);

        DB::table('ulists')->insert([
            'name' => "grocery",
            'user_id' => 1     
        ]);

        DB::table('tasks')->insert([
            'name' => "walk",
            'ulist_id' => 1     
        ]);

        DB::table('tasks')->insert([
            'name' => "run",
            'ulist_id' => 2     
        ]);

    }
}
