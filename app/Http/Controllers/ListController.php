<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ListController extends Controller
{
	public function index(){
		
		$home = "All";
		$user = \App\User::find(1);
    	$tasks = \App\Task::all();
        $lister = \App\Ulist::all();
        $ul = $user->ulists()->get();
        $lt = $lister->first()->tasks()->get();
        $ut = $user->tasks()->get();

    	return view('home',['user' => $user,'tasks' => $tasks,'lister' => $lister,'ul' => $ul,'lt' => $lt,'ut' => $ut,'list_t' => $home]);
	}

	public function create_new(){
		
		$user = \App\User::find(1);
    	$tasks = \App\Task::all();
        $lister = \App\Ulist::all();
        $ul = $user->ulists()->get();
        $lt = $lister->first()->tasks()->get();
        $ut = $user->tasks()->get();

    	return view('home',['user' => $user,'tasks' => $tasks,'lister' => $lister,'ul' => $ul,'lt' => $lt,'ut' => $ut]);
	}
}
