<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>TY TDL-@yield('title')</title>
  </head>
  <body>
  	<div class="main-container">
  		@yield('content')
  	</div>
  </body>
</html>